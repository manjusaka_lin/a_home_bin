#coding=utf-8
import os
import pickle
import codecs
import sys
import re
import operator
import random


#INSERT INTO `jlinkSwSuiteCom`.`hwinfo_config` (`sca_project`, `hw_identity`, `hw_value`, `hw_type`, `info`) VALUES ('common', 'back_cam_interpolation', '62', 'hardware_info', '后摄差值');

#INSERT INTO `jlinkSwSuiteCom`.`hwinfo_item` (`sca_project`, `hw_identity`, `hw_value`, `hw_type`) VALUES ('common', 'DEFAULT', '0', 'back_cam_interpolation');

#ALTER TABLE hwinfo_new_sw_project_map2_sca_project ADD COLUMN sub_emmc_speed VARCHAR(100) NOT NULL DEFAULT 'common';
#ALTER TABLE `jlinkSwSuiteCom`.`hwinfo_new_sw_project_map2_sca_project` ADD CONSTRAINT `CON_emmc_speed` FOREIGN KEY (`sub_emmc_speed`) REFERENCES `jlinkSwSuiteCom`.`hwinfo_sca_project`(`sca_project`) ON UPDATE CASCADE;


colname=sys.argv[1];
colidx=sys.argv[2];
outputsql='''
INSERT INTO `jlinkSwSuiteCom`.`hwinfo_config` (`sca_project`, `hw_identity`, `hw_value`, `hw_type`, `info`) VALUES ('common', '%s', '%s', 'hardware_info', 'dummy');

INSERT INTO `jlinkSwSuiteCom`.`hwinfo_item` (`sca_project`, `hw_identity`, `hw_value`, `hw_type`) VALUES ('common', 'DEFAULT', '0', '%s');

ALTER TABLE hwinfo_new_sw_project_map2_sca_project ADD COLUMN sub_%s VARCHAR(100) NOT NULL DEFAULT 'common';

ALTER TABLE `jlinkSwSuiteCom`.`hwinfo_new_sw_project_map2_sca_project` ADD CONSTRAINT `CON_%s` FOREIGN KEY (`sub_%s`) REFERENCES `jlinkSwSuiteCom`.`hwinfo_sca_project`(`sca_project`) ON UPDATE CASCADE;
ALTER TABLE hwinfo_sca ADD COLUMN %s VARCHAR(100) NOT NULL DEFAULT 'DEFAULT';
''' % (colname, colidx, colname, colname, colname, colname, colname)
print outputsql

