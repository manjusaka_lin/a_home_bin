
#PRODUCT_PARAMETER_TEMPLATE=device/rockchip/common/scripts/parameter_tools/parameter.in
PRODUCT_PARAMETER_TEMPLATE=./parameter.in
BOARD_PLATFORM_VERSION="10.0"
PRODUCT_MODEL=RK3399
PRODUCT_MANUFACTURER=RK3399
PRODUCT_DEVICE=3399
rebuild_parameter=./parameter.txt
#for n102
#partition_list=uboot:4M,trust:4M,misc:4M,proinfo:4M,dtb:4M,dtbo:4M,vbmeta:1M,boot:32M,security:4M,recovery:96M,backup:112M,cache:384M,metadata:16M,frp:512K,super:3112M

#for x219
# 219 using
partition_list=uboot:4M,trust:4M,misc:4M,dtb:4M,dtbo:4M,proinfo:4M,vbmeta:1M,boot:32M,recovery:96M,backup:112M,security:4M,cache:384M,metadata:16M,frp:512K,super:3112M


./parameter_tools --input $PRODUCT_PARAMETER_TEMPLATE \
	--firmware-version $BOARD_PLATFORM_VERSION \
	--machine-model $PRODUCT_MODEL \
	--manufacturer $PRODUCT_MANUFACTURER \
	--machine $PRODUCT_DEVICE \
	--partition-list $partition_list \
	--output $rebuild_parameter


rebuild_parameter=./parameter_ab.txt
# 219 a/b
partition_list=uboot_a:4M,trust_a:4M,misc:4M,proinfo:4M,dtb:4M,dtbo_a:4M,vbmeta_a:1M,boot_a:96M,backup:112M,security:4M,cache:384M,metadata:16M,frp:512K,super:6224M
#partition_list=uboot_a:4M,trust_a:4M,misc:4M,dtb:4M,dtbo_a:4M,vbmeta_a:1M,boot_a:96M,backup:112M,security:4M,cache:384M,metadata:16M,frp:512K,super:6224M
./parameter_tools --input $PRODUCT_PARAMETER_TEMPLATE \
	--firmware-version $BOARD_PLATFORM_VERSION \
	--machine-model $PRODUCT_MODEL \
	--manufacturer $PRODUCT_MANUFACTURER \
	--machine $PRODUCT_DEVICE \
	--partition-list $partition_list \
	--output $rebuild_parameter


