
#PRODUCT_PARAMETER_TEMPLATE=device/rockchip/common/scripts/parameter_tools/parameter.in
PRODUCT_PARAMETER_TEMPLATE=./parameter.in
BOARD_PLATFORM_VERSION="11.0"
PRODUCT_MODEL=rkg185_3566_64_r
PRODUCT_MANUFACTURER=rockchip
PRODUCT_DEVICE=rkn102_3368_64_r
rebuild_parameter=./parameter.txt
#for n102
partition_list=uboot:4M,trust:4M,misc:4M,proinfo:4M,dtb:4M,dtbo:4M,vbmeta:1M,boot:32M,security:4M,recovery:96M,backup:112M,cache:384M,metadata:16M,frp:512K,super:3112M
#partition_list=uboot:4M,trust:4M,misc:4M,dtb:4M,dtbo:4M,vbmeta:1M,boot:32M,security:4M,recovery:96M,backup:112M,cache:384M,metadata:16M,frp:512K,super:3112M

#CMDLINE:mtdparts=rk29xxnand:0x00002000@0x00004000(uboot),0x00002000@0x00006000(trust),0x00002000@0x00008000(misc),0x00002000@0x0000a000(dtb),0x00002000@0x0000c000(dtbo),0x00000800@0x0000e000(vbmeta),0x00010000@0x0000e800(boot),0x00002000@0x0x0001e800(security),0x00030000@0x00020800(recovery),0x00038000@0x00050800(backup),0x000c0000@0x00088800(cache),0x00008000@0x00148800(metadata),0x00000400@0x00150800(frp),0x00614000@0x00150c00(super),-@0x00764c00(userdata:grow)


./parameter_tools --input $PRODUCT_PARAMETER_TEMPLATE \
	--firmware-version $BOARD_PLATFORM_VERSION \
	--machine-model $PRODUCT_MODEL \
	--manufacturer $PRODUCT_MANUFACTURER \
	--machine $PRODUCT_DEVICE \
	--partition-list $partition_list \
	--output $rebuild_parameter

