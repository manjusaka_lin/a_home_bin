mount -o bind /dev /mnt/dev
mount -o bind /proc /mnt/proc
mount -o bind /sys /mnt/sys
mount -o bind /run /mnt/run

chroot /mnt/
blkid /dev/sda1

echo "modif /etc/fstab for mount correct uuid"
grub-install /dev/sdxsx
upgrade-grub
exit


