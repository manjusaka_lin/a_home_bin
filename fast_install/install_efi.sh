#!/bin/bash
echo "usage: bash xx.sh /dev/sdx(for root) /dev/sdy(for home) /dev/sdz(for swap) /xx/zz/root.sfs /xx/home.sfs"
EFIDEV=$1
esfs=$2
echo $#
if [[ $# < 2 ]];then
	echo "ERROR please read usage!!!"
	exit
fi

read -r -p "efi sfs is from $2, Are You Sure? [Y/n] " input
echo -e "efi install to \033[31m $1\033[0m "
read -r -p "Are You Sure? [Y/n] " input
case $input in
    [yY][eE][sS]|[yY])
		echo "Yes"
		;;

    [nN][oO]|[nN])
		echo "No"
		exit
       	;;

    *)
		echo "Invalid input..."
		exit 1
		;;
esac

if [[ $EFIDEV == /dev/sd* ]] ;then
#if [[ $ROOTDEV == /dev/sd* ]];then
	echo -e "use \033[32m $EFIDEV \033[0m"
else
	echo "INVALID DEV"
	exit;
fi


rdev=${EFIDEV: 0: 8}
echo $rdev

#sudo su
echo "NOW START mkfs"
echo "y" | sudo mkfs.vfat $EFIDEV

echo "SECOND copying efi SFS..."
sudo umount /jmnt
sudo umount /jrecovery
sudo mkdir /jmnt
sudo mkdir /jrecovery
sudo mount $EFIDEV /jmnt
sudo mount -o loop $esfs /jrecovery
sudo cp -a /jrecovery/* /jmnt/
sudo umount /jmnt
sudo umount /jrecovery
exit
