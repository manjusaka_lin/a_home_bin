#!/bin/bash
echo "usage: bash xx.sh /dev/sdx(for root) /dev/sdy(for home) /dev/sdz(for swap) /xx/zz/root.sfs /xx/home.sfs"
ROOTDEV=$1
HOMEDEV=$2
SWAPDEV=$3
rsfs=$4
hsfs=$5
echo $#
if [[ $# < 5 ]];then
	echo "ERROR please read usage!!!"
	exit
fi

read -r -p "rootsfs is from $4, home is from $5, Are You Sure? [Y/n] " input
echo -e "root install to \033[31m $1\033[0m , home install to \033[31m$2\033[0m, swap is \033[31m$3\033[0m"
read -r -p "Are You Sure? [Y/n] " input
case $input in
    [yY][eE][sS]|[yY])
		echo "Yes"
		;;

    [nN][oO]|[nN])
		echo "No"
		exit
       	;;

    *)
		echo "Invalid input..."
		exit 1
		;;
esac

#if [[ $ROOTDEV == /dev/sd* ]] && [[ $HOMEDEV == /dev/sd* ]] && [[ $SWAPDEV == /dev/sd* ]];then
##if [[ $ROOTDEV == /dev/sd* ]];then
	#echo -e "use \033[32m $ROOTDEV $HOMEDEV $SWAPDEV\033[0m"
#else
	#echo "INVALID DEV"
	#exit;
#fi


#rdev=${ROOTDEV: 0: 8}
rdev=$6
echo $rdev

#sudo su
echo "NOW START mkfs"
echo "y" | sudo mkfs.ext4 $ROOTDEV
echo "y" | sudo mkfs.ext4 $HOMEDEV
echo "y" | sudo mkswap $SWAPDEV

rid=$(sudo blkid | grep $ROOTDEV | awk -F"\"" '{print $2}')
hid=$(sudo blkid | grep $HOMEDEV | awk -F"\"" '{print $2}')
sid=$(sudo blkid | grep $SWAPDEV | awk -F"\"" '{print $2}')
echo "$rid $hid $sid"
if [[ $rid == "" ]]; then
	echo "invalid $ROOTDEV"
	exit
fi

if [[ $hid == "" ]]; then
	echo "invalid $HOMEDEV"
	exit
fi
if [[ $sid == "" ]]; then
	echo "invalid $SWAPDEV"
	exit
fi

echo "# /etc/fstab: static file system information." > fstab
echo "# Use 'blkid' to print the universally unique identifier for a">>fstab
echo "# device; this may be used with UUID= as a more robust way to name devices" >> fstab
echo "# that works even if disks are added and removed. See fstab(5).">>fstab
echo "#">>fstab
echo "# <file system> <mount point>   <type>  <options>       <dump>  <pass>" >> fstab
echo "UUID=$rid /	ext4	errors=remount-ro  0  1" >> fstab
echo "UUID=$hid /home	ext4	defaults	0	 2" >> fstab
echo "UUID=$sid none	swap	sw	 0	 0">> fstab


echo "SECOND copying home SFS..."
sudo mkdir /jmnt
sudo mkdir /jrecovery
sudo mount $HOMEDEV /jmnt
sudo mount -o loop $hsfs /jrecovery
sudo cp -a /jrecovery/* /jmnt/
sudo umount /jmnt
sudo umount /jrecovery

echo "THIRD: start copy root sfs...."
sudo umount /jmnt
sudo umount /jrecovery
#sudo mkfs.ext4 $ROOTDEV
sudo mount $ROOTDEV /jmnt
sudo mount -o loop $rsfs /jrecovery
sudo cp -a /jrecovery/* /jmnt/

sudo cp fstab /jmnt/etc/fstab

(echo "set 1 boot off; set 1 bios_grub on; q")| sudo parted $rdev
sudo mount -o bind /dev /jmnt/dev
sudo mount -o bind /proc /jmnt/proc
sudo mount -o bind /sys /jmnt/sys
sudo mount -o bind /run /jmnt/run

echo "4th to install grup on $rdev"
(echo "grub-install $rdev;update-grub2;sync;exit")| sudo chroot /jmnt/
#sudo grub-install $rdev
#sudo upgrade-grub
#sudo sync
sudo umount /jmnt/dev
sudo umount /jmnt/proc
sudo umount /jmnt/sys
sudo umount /jmnt/run

exit
