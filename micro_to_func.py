#coding=utf-8
import os
import sys

print '''usage: python xx.py yy_class file
            yy_class: support: define|enum
            like: python xx.py define aa.c
'''
input_f=sys.argv[2];
if input_f.find("/") != -1:
    input_dir=input_f[:input_f.rfind('/')]
else:
    input_dir="./"
print input_dir
f=open(input_f, 'r');
lines=f.readlines();

strclass = sys.argv[1]
def gen_str_from_enum_func(lines):
    outstr="char *jlink_get_str_from_enum(int tag)\n{\n\tchar *ret_str=NULL;\n\tswitch(tag){\n"
    mmap_str=""
    for cline in lines:
        cline = cline.strip();
        if cline.find(",") != -1:
            cline=cline.split(",")[0];
        if cline.find("=") != -1:
            cline=cline.split("=")[0];

        if cline.find("enum") != -1:
            continue;
        cline = cline.strip();
        if (len(cline) <= 0):
            continue;
        if cline[0] == '/':
            continue;
        if cline[0] == '}':
            continue;
        if cline[0] == '#':
            continue;
        if cline[0] == '*':
            continue;

        if cline[len(cline)-1] == '{':
            continue;

        # print cline
        outstr="%s\n\tcase %s:\n\t\tret_str=\"%s\";\n\t\tbreak;"%(outstr, cline, cline)
        mmap_str="%s\tenum_mMap.add(String8(\"%s\"), %s);\n" % (mmap_str, cline, cline);

    outstr="%s\n\tdefault:\n\t\tret_str=\"UNKONW\";\n\t\tbreak;\n\t}\n\treturn ret_str;\n}\n"%(outstr)
    print outstr
    print mmap_str;
    out_file=input_dir + "/jlink_code.c"
    genf=open(out_file, 'w+');
    genf.write(outstr);
    genf.close();

def gen_str_from_define_func(lines):
    outstr="char *jlink_get_str_from_define(int tag)\n{\n\tchar *ret_str=NULL;\n\tswitch(tag){\n"
    for cline in lines:
        cline = cline.strip();
        if (len(cline) <= 0):
            continue;

        if cline[0] == '/':
            continue;
        if cline[0] != '#':
            continue;

        if cline.find(" ") != -1:
            cline=cline.split(" ")[1];
        if cline.find("\t") != -1:
            cline=cline.split("\t")[0];

#         if cline.find("=") != -1:
            # cline=cline.split("=")[0];
        cline = cline.strip();
        if (len(cline) <= 0):
            continue;

        print cline
        outstr="%s\n\tcase %s:\n\t\tret_str=\"%s\";\n\t\tbreak;"%(outstr, cline, cline)

    outstr="%s\n\tdefault:\n\t\tret_str=\"UNKONW\";\n\t\tbreak;\n\t}\n\treturn ret_str;\n}\n"%(outstr)
    print outstr
    out_file=input_dir + "/jlink_code_define.c"
    genf=open(out_file, 'w+');
    genf.write(outstr);
    genf.close();


if strclass == "define":
    gen_str_from_define_func(lines)
if strclass == "enum":
    gen_str_from_enum_func(lines)
