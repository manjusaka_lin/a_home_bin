
#PRODUCT_PARAMETER_TEMPLATE=device/rockchip/common/scripts/parameter_tools/parameter.in
PRODUCT_PARAMETER_TEMPLATE=./parameter.in
./parameter_tools --input $(PRODUCT_PARAMETER_TEMPLATE) \
	--firmware-version $(BOARD_PLATFORM_VERSION) \
	--machine-model $(PRODUCT_MODEL) \
	--manufacturer $(PRODUCT_MANUFACTURER) \
	--machine $(PRODUCT_DEVICE) \
	--partition-list $(partition_list) \
	--output $(rebuild_parameter)

