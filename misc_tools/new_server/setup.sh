#sudo apt update
#https://www.cnblogs.com/conscience-remain/p/17209432.html
#https://www.gerritcodereview.com/3.7.html

sudo adduser gerrit
sudo mkdir /gitsrv
sudo chown gerrit:gerrit /gitsrv -R
(echo "Y") |sudo apt install openjdk-11-jdk
(echo "Y") |sudo apt install git gitweb apache2
(echo "Y") |sudo apt-get install nginx
sudo su gerrit
java -jar gerrit*.war init -d ~/Gerrit/
echo "add admin for gerrit"
htpasswd -c ~/Gerrit/etc/gerrit.password admin   # 创建第一个用户admin，同时会生成一个gerrit.password文件
 
echo "add one user lyn for gerrit"
htpasswd -m ~/Gerrit/etc/gerrit.password lyn     # 在gerrit.password增加用户用 -m
cp ./gerrit.config_etc2 ~/Gerrit/etc/gerrit.config

sudo /etc/init.d/apache2 stop
#perl -pe 's/[^[:ascii:]]//g' ./gerrit.conf_nginx
sudo cp ./gerrit.conf_nginx2 /etc/nginx/conf.d/gerrit.conf

sudo netstat -ltpn
#ps $(fuser 8088/tcp)这个工具可以直接查看有没有占据这个端口的进程

sudo sed -i 's/user www-data;/user root;/g' /etc/nginx/nginx.conf
#kms 出现 500的报错： 500 Internal Server Error
# 查看log vi /var/log/nginx/error.log  是 13 没有权限，
# ps aux | grep nginx ,查看是www-data的用户名，修改 sudo vi /etc/nginx/nginx.conf
# user root;
# 重启即可：sudo systemctl restart nginx
#https://blog.csdn.net/qq_26545503/article/details/119335691

#~/Gerrit/bin/gerrit.sh start

sudo systemctl restart nginx
sudo cp ./gerrit.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable gerrit
sudo systemctl start gerrit
sudo journalctl -u gerrit


#for gitweb
#sudo vi /etc/apache2/ports.conf change to 82
#sudo apt-get install apache2 apache2-utils git-core gitweb
#add gitweb to gerrit.config 中即可
