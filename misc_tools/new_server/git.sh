#sudo apt update
#https://www.cnblogs.com/conscience-remain/p/17209432.html
#https://www.gerritcodereview.com/3.7.html
sudo apt install openjdk-11-jdk
sudo apt install git gitweb apache2
sudo adduser gerrit
java -jar gerrit*.war init -d ~/Gerrit/
htpasswd -c /home/test/Gerrit/etc/gerrit.password admin   # 创建第一个用户admin，同时会生成一个gerrit.password文件
 
htpasswd -m /home/test/Gerrit/etc/gerrit.password lyn     # 在gerrit.password增加用户用 -m

sudo apt-get install nginx
sudo apt install gitolite3
sudo /etc/init.d/apache2 stop
sudo cp ./gerrit.conf_nginx /etc/nginx/conf.d/gerrit.conf
#设置nginx的gerrit反向代理文件 sudo vi /etc/nginx/conf.d/gerrit.conf
#配置nginx反向代理，文件内容如下
#server {
    #listen 81;
    #server_name localhost;
    #allow all;
    #deny all;

    #auth_basic "Welcome to Gerrit Code Review Site!";
    #auth_basic_user_file /home/lyn/Gerrit/etc/gerrit.password;


    #location / {
        #proxy_pass http://127.0.0.1:8088;
        #proxy_set_header X-Forwarded-For $remote_addr;
        #proxy_set_header Host $host;
    #}
#}


#其中还修改了gerrit中etc目录的gerrit.config文件，内容如下：
#[gerrit]
#        basePath = git
#        canonicalWebUrl = http://lyn:8088/
#        serverId = f0858671-052c-4ea0-9883-032a598c9aae
#[container]
#        javaOptions = "-Dflogger.backend_factory=com.google.common.flogger.backend.log4j.Log4jBackendFactory#getInstance"
#        javaOptions = "-Dflogger.logging_context=com.google.gerrit.server.logging.LoggingContext#getInstance"
#        user = lyn
#        javaHome = /usr/lib/jvm/java-11-openjdk-amd64
#[index]
#        type = lucene
#[auth]
#        type = HTTP
#[receive]
#        enableSignedPush = false
#[sendemail]
#        smtpServer = localhost
#[sshd]
#        listenAddress = *:29418
#[httpd]
#        listenUrl = proxy-https://*:8088/
#[cache]
#        directory = cache


sudo netstat -ltpn
#ps $(fuser 8088/tcp)这个工具可以直接查看有没有占据这个端口的进程

#kms 出现 500的报错： 500 Internal Server Error
# 查看log vi /var/log/nginx/error.log  是 13 没有权限，
# ps aux | grep nginx ,查看是www-data的用户名，修改 sudo vi /etc/nginx/nginx.conf
# user root;
# 重启即可：sudo systemctl restart nginx
#https://blog.csdn.net/qq_26545503/article/details/119335691
~/Gerrit/bin/gerrit.sh start
sudo systemctl restart nginx



#如何 gitsrv 目录是后面建的， 要把 ALL-Projects all-users复制到 GIT 目录里。owner是运行 gerrit的用户
