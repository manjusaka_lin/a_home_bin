#/bin/sh
#1.mysql -hhostname -uuser -ppsword -e "mysql_cmd"
#2.

#mysql -hhostname -uuser -ppsword << EOF
    #mysql_cmd
#EOF
#CREATE DATABASE example_database;

mysql -e "CREATE DATABASE example_database;"
mysql << EOF
CREATE USER 'example_user'@'%' IDENTIFIED WITH mysql_native_password BY '!ExTest123';
GRANT ALL ON example_database.* TO 'example_user'@'%';
EOF
mysql << EOF
SHOW DATABASES;
CREATE TABLE example_database.example_data (
	item_id INT AUTO_INCREMENT,
	content VARCHAR(255),
	PRIMARY KEY(item_id)
);
 INSERT INTO example_database.example_data (content) VALUES ("第一筆測試資料");
 INSERT INTO example_database.example_data (content) VALUES ("第二筆測試資料");
 INSERT INTO example_database.example_data (content) VALUES ("第三筆測試資料");
SELECT * FROM example_database.example_data;
EOF
mysql << EOF
CREATE USER 'wifig2030'@'%' IDENTIFIED WITH mysql_native_password BY '!Wifig2030';
GRANT ALL ON wifidb.* TO 'wifig2030'@'%';
EOF

