sudo apt update;
sudo apt install nginx
systemctl status nginx

sudo apt install mysql-server
sudo mysql_secure_installation

sudo apt install php8.1-fpm php-mysql
sudo mkdir -p /var/www/test3.ui-code.com/html

USER=$(whoami)
sudo chown -R $USER:$USER /var/www/test3.ui-code.com/html
sudo chmod -R 755 /var/www/test3.ui-code.com
sudo cp ./index.html /var/www/test3.ui-code.com/html/
sudo cp ./test3.ui-code.com /etc/nginx/sites-available/
#以下是這些指示和位置區塊的功能：
#
#listen：定義 Nginx 監聽的埠號。在這個情況下，它會監聽 80 埠，這是 HTTP 的預設埠號。
#root：定義此網站所提供檔案的文件根目錄。
#index：定義 Nginx 為此網站優先選擇的索引檔案順序。通常的做法是將 index.html 檔案列在 index.php 檔案之前，以便在 PHP 應用程式中快速設置維護導引頁面。你可以調整這些設定以更好地適應你的應用程式需求。
#server_name：定義此伺服器區塊應該回應的域名和/或 IP 地址。將此指示指向你的伺服器域名或公共 IP 地址。
#location /：第一個位置區塊包含 try_files 指示，它會檢查是否存在與 URL 請求匹配的檔案或目錄。如果 Nginx 找不到適當的資源，將返回 404 錯誤。
#location ~ .php$：此位置區塊通過將 Nginx 指向 fastcgi-php.conf 設定檔和 php8.1-fpm.sock 檔案，處理實際的 PHP 處理過程，後者宣告了與 php8.1-fpm 相關的插座。
#location ~ /.ht：最後一個位置區塊處理 .htaccess 檔案，Nginx 不處理這些檔案。通過添加 deny all 指示，如果任何 .htaccess 檔案意外進入文件根目錄，它們將不會提供給訪客。

sudo ln -s /etc/nginx/sites-available/test3.ui-code.com /etc/nginx/sites-enabled/
sudo unlink /etc/nginx/sites-enabled/default


sudo vi /etc/nginx/nginx.conf
#找到 server_names_hash_bucket_size 指令，移除 # 符號以解除註解。如果你使用的是 nano 編輯器，你可以按下 Ctrl 和 w 鍵快速在檔案中搜尋詞語。

sudo nginx -t
sudo systemctl restart nginx

sudo cp ./info.php /var/www/test3.ui-code.com/html/


#  "
#  sudo mysql
#  CREATE DATABASE example_database;
#  CREATE USER 'example_user'@'%' IDENTIFIED WITH mysql_native_password BY '!ExTest123';
#  GRANT ALL ON example_database.* TO 'example_user'@'%';
#  #exit
#   mysql -u example_user -p
#  SHOW DATABASES;
#  CREATE TABLE example_database.example_data (
#  	item_id INT AUTO_INCREMENT,
#  	content VARCHAR(255),
#  	PRIMARY KEY(item_id)
#  );
#  "
#  
#  '''
#   INSERT INTO example_database.example_data (content) VALUES ("第一筆測試資料");
#   INSERT INTO example_database.example_data (content) VALUES ("第二筆測試資料");
#   INSERT INTO example_database.example_data (content) VALUES ("第三筆測試資料");
#  SELECT * FROM example_database.example_data;
#  '''
sudo cp ./example_data.php  /var/www/test3.ui-code.com/html/
