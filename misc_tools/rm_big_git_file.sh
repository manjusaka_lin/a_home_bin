#https://blog.csdn.net/Bruce1114/article/details/125595913
# 列出最大的5个文件和对应的提交记录ID
git rev-list --objects --all | grep "$(git verify-pack -v .git/objects/pack/*.idx | sort -k 3 -n | tail -5 | awk '{print$1}')"
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch docker-file/jdk/fonts.tar.gz' --prune-empty --tag-name-filter cat -- --all
# 删除original文件夹
rm -rf .git/refs/original/
# 修剪旧的reflog条目
git reflog expire --expire=now --all
# 验证数据库中对象的连接性和有效性
git fsck --full --unreachable
# 打包解压缩的对象。 删除冗余的对象
git repack -A -d
# 清理不必要的文件并优化本地存储库
git gc --aggressive --prune=now
# 强制推送到远程git仓库
$ git push origin --force --all
