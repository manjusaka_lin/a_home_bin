sudo tee /etc/docker/daemon.json << EOF
{
"registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"]
}
EOF

sudo apt install apt-transport-https ca-certificates curl software-properties-common

for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
sudo rm -rf /var/lib/docker
#1.常归删除操作
sudo apt-get autoremove docker docker-ce docker-engine docker.io containerd runc
 
#2. 删除docker其他没有没有卸载
dpkg -l | grep docker
dpkg -l |grep ^rc | awk ‘{print $2}’ |sudo xargs dpkg -P # 删除无用的相关的配置文件
 
#3.卸载没有删除的docker相关插件(结合自己电脑的实际情况)
sudo apt-get autoremove docker-ce-*
 
#4.删除docker的相关配置&目录
sudo rm -rf /etc/systemd/system/docker.service.d
sudo rm -rf /var/lib/docker
 
#5.确定docker卸载完毕
docker --version

sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update
 
#安装相应的依赖
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo dpkg -i ./ubuntu2204/*.deb
sudo gpasswd -a $USER docker
newgrp docker

sudo mkdir -p /etc/docker
#获取镜像加速器地址
#ACR会为每一个账号（阿里云账号或RAM用户）生成一个镜像加速器地址，配置镜像加速器前，您需要获取镜像加速器地址。
#登录容器镜像服务控制台，在左侧导航栏选择镜像工具 > 镜像加速器，在镜像加速器页面获取加速器地址。
sudo tee /etc/docker/daemon.json << EOF
{
"registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"]
}
EOF
sudo systemctl daemon-reload

##sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
#sudo cp docker.asc /etc/apt/keyrings/docker.asc
#sudo chmod a+r /etc/apt/keyrings/docker.asc


#sudo add-apt-repository \
   #"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   #$(lsb_release -cs) \
   #stable"

# Add the repository to Apt sources:
#echo \
  #"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  #$(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  #sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#sudo apt-get update

#sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
#sudo docker cp ./mysql-connector-j-8.0.33.jar  jira1:/opt/atlassian/jira/lib/
#docker exec -it jira /bin/bash
