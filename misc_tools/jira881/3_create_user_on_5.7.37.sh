#/bin/sh
#1.mysql -hhostname -uuser -ppsword -e "mysql_cmd"
#2.

#mysql -hhostname -uuser -ppsword << EOF
    #mysql_cmd
#EOF
#CREATE DATABASE example_database;

echo "create db"
mysql -u root -p -e "CREATE DATABASE jiradb CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;"
echo "create user"
mysql -u root -p -e "UNINSTALL PLUGIN validate_password;"
mysql -u root -p << EOF
CREATE USER 'jira'@'%' IDENTIFIED BY 'jira';
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,REFERENCES,ALTER,INDEX on jiradb.* TO 'jira'@'%';
EOF
echo "updated on 5.7.6+"

#for v8.0
#GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, ALTER, INDEX ON jiradb.* TO 'jira'@'%';
mysql -u root -p << EOF
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'jira'@'%'
EOF

sudo cp my.cnf /etc/mysql/
