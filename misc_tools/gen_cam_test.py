#coding=utf-8
import os
import sys


#for rk3399
# the size must form adb shell dumpsys media.camera
# otherwise it will error: rkisp1: CIF_ISP_PIC_SIZE_ERROR (0x00000001)

camid="m00_b_ov13850 1-003c"
wid=2112
hei=1568
#linkup
outstr="media-ctl -r\n"
outstr='''%s\nmedia-ctl -l '"%s":0->"rockchip-mipi-dphy-rx":0[1]'
'''% (outstr, camid);

outstr='''%s
media-ctl -l '"rockchip-mipi-dphy-rx":1->"rkisp1-isp-subdev":0[1]'
''' % outstr

outstr='''%s
media-ctl -l '"rkisp1-input-params":0->"rkisp1-isp-subdev":1[1]'
''' % outstr
outstr='''%s
media-ctl -l '"rkisp1-isp-subdev":2->"rkisp1_selfpath":0[1]'
''' % outstr
outstr='''%s
media-ctl -l '"rkisp1-isp-subdev":2->"rkisp1_mainpath":0[1]'
''' % outstr
outstr='''%s
media-ctl -l '"rkisp1-isp-subdev":3->"rkisp1-statistics":0[1]'
''' % outstr

#set size

outstr='''%s
media-ctl --set-v4l2 '"%s":0[fmt:SBGGR10/%sx%s]'
''' % (outstr, camid, wid, hei);
outstr='''%s
media-ctl --set-v4l2 '"rkisp1-isp-subdev":0[fmt:SBGGR10/%sx%s]'
''' % (outstr, wid, hei)
outstr='''%s
media-ctl --set-v4l2 '"rkisp1-isp-subdev":0[fmt:SBGGR10/%sx%s]' --set-v4l2 '"rkisp1-isp-subdev":0[crop:(0,0)/%sx%s]'
''' % (outstr, wid, hei, wid, hei)
outstr='''%s
media-ctl --set-v4l2 '"rkisp1-isp-subdev":2[fmt:YUYV2X8/%sx%s]'
''' % (outstr, wid, hei)
outstr='''%s
media-ctl --set-v4l2 '"rkisp1-isp-subdev":2[fmt:SBGGR10/%sx%s]' --set-v4l2 '"rkisp1-isp-subdev":2[crop:(0,0)/%sx%s]'
''' % (outstr, wid, hei, wid, hei)

#to test size setuped
outstr='''%s
media-ctl --get-v4l2 '"rkisp1-isp-subdev":0'
''' % outstr

#do capture
outstr='''%s
v4l2-ctl --verbose -d /dev/video0 --set-fmt-video=width=%s,height=%s,pixelformat='NV12' --stream-mmap=4 --set-selection=target=crop,flags=0,top=0,left=0,width=2592,height=1944
''' % (outstr, wid, hei)

print outstr
fname="%sx%s.sh" % (wid,hei)
ff=open(fname, "w+")
ff.write(outstr)
ff.close()
