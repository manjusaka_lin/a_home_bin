#coding=utf-8
import re
import sys

def parse_partition_logs_from_file(file_path):
    """
    从文件读取分区日志并生成分区配置文本

    :param file_path: str, 包含分区日志的文件路径
    :return: str, 分区配置文本
    """
    # 定义正则表达式匹配每行日志
    pattern = r"name=(\w+), start_sect=(0x[0-9a-fA-F]+), nr_sects=(0x[0-9a-fA-F]+), uuid=([\w-]+)"

    # 结果列表
    partition_configs = []

    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()

        # 遍历每行日志
        for index, line in enumerate(lines):
            match = re.search(pattern, line)
            if match:
                partition_name, start_sect, nr_sects, uuid = match.groups()

                # 计算线性起始地址和分区大小（每扇区大小为 512 字节）
                start_addr = int(start_sect, 16) * 512
                partition_size = int(nr_sects, 16) * 512

                # 构造分区配置
                config = f'''- partition_index: SYS{index+2}
  partition_name: {partition_name}
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: {hex(start_addr)}
  physical_start_addr: {hex(start_addr)}
  partition_size: {hex(partition_size)}
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

'''
                partition_configs.append(config)
    except FileNotFoundError:
        return f"Error: File '{file_path}' not found."
    except Exception as e:
        return f"Error: {str(e)}"

    # 合并所有分区配置并返回
    return "\n".join(partition_configs)

if __name__ == "__main__":
    # 检查是否提供了文件名
    if len(sys.argv) != 2:
        print("Usage: python script.py <log_file>")
        sys.exit(1)

    # 获取文件名并解析日志
    file_path = sys.argv[1]
    result = parse_partition_logs_from_file(file_path)
    print(result)
