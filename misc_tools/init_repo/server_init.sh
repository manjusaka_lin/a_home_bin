tdir=$1
srvdir=$(pwd)

cd $srvdir/$tdir
varlist=$(cat dirlist)
for cdir in $varlist; do
       ##echo $cdir
       echo $(basename $cdir)
       ccdir=$(basename $cdir)
       git init --bare $ccdir.git
done
sudo chown git:git $srvdir/$tdir/* -R
cd -
