srvdir=/gitsrv/reposm6877
dirlist="
art/
bionic/
bootable/
build/
ccu_tool/
compatibility/
cts/
dalvik/
developers/
development/
device/
external/
frameworks/
hardware/
kernel-4.14/
kernel-4.19-lc/
kernel-4.19/
kernel-5.10/
kernel/
libcore/
libnativehelper/
out/
packages/
pdk/
platform_testing/
prebuilts/
sdk/
system/
test/
toolchain/
tools/
vendor/
"

cd $srvdir
for cdir in $dirlist; do
       ##echo $cdir
       #echo $(basename $cdir)
       ccdir=$(basename $cdir)
       git init --bare $ccdir.git
done
sudo chown git:git $srvdir/* -R
cd -

