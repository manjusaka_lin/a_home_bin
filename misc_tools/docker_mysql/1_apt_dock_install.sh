sudo apt-get install docker.io -y

sudo cp ./daemon.json /etc/docker/

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo cp ./http-proxy.conf /etc/systemd/system/docker.service.d/ 
sudo systemctl daemon-reload
sudo systemctl restart docker
#sudo systemctl status docker
sudo systemctl show --property=Environment docker
