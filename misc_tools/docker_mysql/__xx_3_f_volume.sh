#basicdirs=/home/bsp/8tdata/
sudo docker volume rm mysql5737-jira 
sudo docker volume create mysql5737-jira 
basicdirs=/usr/local/
# 宿主机创建数据存放目录映射到容器
#sudo rm -rf $basicdirs/docker_data
sudo mkdir -p $basicdirs/docker_data/mysql/data
# 宿主机创建配置文件目录映射到容器 
sudo mkdir -p $basicdirs/docker_data/mysql/conf #(需要在此目录下创建"conf.d"、"mysql.conf.d"两个目录)
sudo mkdir -p $basicdirs/docker_data/mysql/conf/conf.d # (建议在此目录创建my.cnf文件并进行相关MySQL配置)
sudo mkdir -p $basicdirs/docker_data/mysql/conf/mysql.conf.d
# 宿主机创建日志目录映射到容器
sudo mkdir -p $basicdirs/docker_data/mysql/logs

#sudo touch $basicdirs/docker_data/mysql/data/ib_logfile0 
#sudo cp ./my.cnf $basicdirs/docker_data/mysql/conf/
#sudo ln -s $basicdirs/docker_data /usr/local/

#sudo mkdir -p $basicdirs/docker_data/mysql/{data,conf,logs}
#sudo chown -R 999:999 $basicdirs/docker_data/mysql
#sudo chmod -R 777 $basicdirs/docker_data/mysql
sudo docker run --privileged=true -d --name mysql-5.7.37 \
	-p 3306:3306 \
	-e MYSQL_ROOT_PASSWORD=kms123456 \
	-v mysql5737-jira:/var/lib/mysql \
	-v /home/bsp/bin/misc_tools/docker_mysql/my.cnf:/etc/mysql/my.cnf \
	mysql:5.7.37

