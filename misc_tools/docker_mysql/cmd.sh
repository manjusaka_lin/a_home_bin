
# 删除容器(参数可以是容器名称或容器ID)
docker rm mysql5.7
# 删除镜像
docker rmi mysql5.7
# 启动容器
docker start mysql5.7
# 启动容器/重启容器
docker restart mysql5.7
# 停止容器
docker stop mysql5.7
# 进入容器
docker exec -it mysql5.7 bash

