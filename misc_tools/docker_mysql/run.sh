#sudo chown -R vboxadd:vboxsf /home/bsp/8tdata/docker_mysql_libs
#sudo chmod 777 -R /home/bsp/8tdata/docker_mysql_libs
#sudo docker run -d --name mysql-5.7.37 \
  #-e MYSQL_ROOT_PASSWORD=kms123456 \
  #-p 3306:3306 \
  #-v /home/bsp/8tdata/docker_mysql_libs/mysql:/var/lib/mysql \
  #-v /home/bsp/bin/misc_tools/docker_mysql/my.cnf:/etc/mysql/my.cnf \
  #mysql:5.7.37

# 宿主机创建数据存放目录映射到容器
sudo mkdir -p /usr/local/docker_data/mysql/data
# 宿主机创建配置文件目录映射到容器 
sudo mkdir -p /usr/local/docker_data/mysql/conf #(需要在此目录下创建"conf.d"、"mysql.conf.d"两个目录)
sudo mkdir -p /usr/local/docker_data/mysql/conf/conf.d # (建议在此目录创建my.cnf文件并进行相关MySQL配置)
sudo mkdir -p /usr/local/docker_data/mysql/conf/mysql.conf.d
# 宿主机创建日志目录映射到容器
sudo mkdir -p /usr/local/docker_data/mysql/logs

sudo cp ./my.cnf /usr/local/docker_data/mysql/conf/
sudo docker run --privileged=true -d --name mysql-5.7.37 \
	-p 3306:3306 \
	-e MYSQL_ROOT_PASSWORD=kms123456 \
	-v /usr/local/docker_data/mysql/data:/var/lib/mysql \
	-v /usr/local/docker_data/mysql/conf:/etc/mysql/ \
	-v /usr/local/docker_data/mysql/logs:/var/log/mysql \
	mysql:5.7.37

