#coding=utf-8
import os
import pickle
import codecs
import sys
import xlrd
import xlwt
import re
import operator
import random
import cairo

#for mtk
def jlink_grep_log_for_batlog(inputlog, grepkey):
    cmd = '''cat -A %s | %s ''' % (inputlog, grepkey)
    print cmd
    batlog = os.popen(cmd).readlines();
    return batlog

def jlink_get_vlist_from_batlog_pre_post(batlog, pre, post):
    relist = [];
    for cline in batlog:
        pr = cline.find(pre);
        if pr != -1:
            nline=cline[pr+len(pre):]
            if len(post) >= 1:
                po = nline.find(post)
                if po != -1:
                    relist.append(nline[:po].rstrip('\\n').strip())
                else:
                    relist.append('0');
            else:
                nline = nline.replace("$","");
                relist.append(nline.rstrip('\\n').strip())
        else:
            relist.append('0');

    return relist

def jlink_get_list_list_from_batlog_pre_post(batlog, pre, post, ksplit):
    relist = [];
    for cline in batlog:
        pr = cline.find(pre);
        if pr != -1:
            nline=cline[pr+len(pre):]
            if len(post) > 1:
                po = nline.find(post)
                if po != -1:
                    relist.append(nline[:po].split(ksplit))
                else:
                    relist.append([]);
            else:
                relist.append(nline.split(ksplit))
        else:
            relist.append([]);

    return relist


def jlink_gen_xls_for_debug(ltime, lsoc):
    list_time = ltime;
    list_soc = lsoc;
    #excel sheet1
    book = xlwt.Workbook(encoding="utf-8")
    sheet1 = book.add_sheet("time-soc")
    for i in range(0, len(list_time)):
        sheet1.write(i, 0, list_time[i]);
    for i in range(0, len(list_soc)):
        sheet1.write(i, 1, list_soc[i]);

    #excel sheet2
    sheet2=book.add_sheet("soc_counts")
    mset={};
    for i in range(0, len(list_soc)):
        mset[list_soc[i]] = 0;
    for i in range(0, len(list_soc)):
        if isinstance(mset[list_soc[i]], int):
            mset[list_soc[i]] = mset[list_soc[i]] + 1;

    sort_item=mset.items();
    sort_item.sort();
    # for key,value in sort_item:
        # print "%s = %d" % (key, value)
    idx=0;
    for key,value in sort_item:
        sheet2.write(idx, 0, key);
        sheet2.write(idx, 1, value);
        idx=idx+1;
    book.save(input_dir + "/gen_xls.xls")
    return sort_item;

#======================================================
def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
#for line png
def jlink_gen_line_data(list_soc, list_time):
    time_soc_list=[];
    for i in range(0, len(list_soc)):
        cur_soc=list_soc[i];
        cur_time=list_time[i];

        if cur_soc.isdigit():
            cur_soc=int(cur_soc);
            if cur_soc > 101:
                cur_soc=101
        else:
            cur_soc=0

        if cur_time.isdigit():
            tmpval=(int(cur_time),cur_soc)
        else:
            tmpval=(0,cur_soc)

        time_soc_list.append(tmpval);
    return time_soc_list

def rk_hwfgu_process(blog, input_dir):
#<6>[   16.880615] c1 sprdbat: bat_log:time:16,vbat:3425,ocv:3274,current:864,cap:0,state:1,auxbat:3431,temp:200
    print "rk hw fgu"
    list_time = jlink_get_vlist_from_batlog_pre_post(blog, '[', '.')
    list_soc = jlink_get_vlist_from_batlog_pre_post(blog, 'mocv:', ',')
    time_soc_list = jlink_gen_line_data(list_soc, list_time)

    # print list_soc
    # print time_soc_list
    print len(list_soc)
    print len(list_time)
    #print list_soc
    interval = len(list_soc)/90;
    idx=interval
    mocvlist=[]
    msoclist=[]
    out_array='j_zero_table = [%d' % int(list_soc[1]);
    out_array='%d' % int(list_soc[-1]);
    mocvlist.append(list_soc[-1])
    while idx < len(list_soc):
        out_array = "%s %d"%(out_array, int(list_soc[-idx]))
        mocvlist.append(list_soc[-idx]);
        idx+= interval;
    for idx in range(len(mocvlist)):
        msoclist.append(idx*100/len(mocvlist));

    out_array = 'j_zero_table = <%s>;'%out_array
    print msoclist
    print out_array



input_f=sys.argv[1];
input_dir=input_f[:input_f.rfind('/')]
print input_dir
if input_f.find('/') == -1:
    print os.getcwd()
    input_dir = os.getcwd();


blog = jlink_grep_log_for_batlog(input_f, 'grep mocv');
# print blog
if len(blog) > 0:
    rk_hwfgu_process(blog, input_dir)





