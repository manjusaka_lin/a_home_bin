SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH
binfile=$1

$SCRIPTPATH/imjtool.ELF64 $binfile extract
mkdir ramdisk
cd ramdisk
gunzip -c ../extracted/ramdisk | cpio -i
