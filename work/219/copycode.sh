
targetdir="/home/manjusaka/fightdata/source_codes/rk_mirror/Rockchip_Android10.0_SDK_Release_default_20210224/"
oridir="/home/manjusaka/all_codes/qrk3399/"
patchdir="/home/manjusaka/eightdata/nnew_work_data/RKX219/code_release/1"
#frameworks
list="bootable/recovery build device hardware/broadcom hardware/rockchip system vendor kernel u-boot"
#for dir in $list; do
	#echo $dir
	##echo $oridir/$dir/* $targetdir/$dir/
	#cp -rf $oridir/$dir/* $targetdir/$dir/
#done
for dir in $list; do
	echo $dir
	cd $targetdir/$dir/;
	mkdir -p $patchdir/$dir/
	git log | grep commit | awk '{x+=1}END{print x}';
	git diff HEAD~ | grep +++ | awk '{print $2}' | sed  's/b\///g' > tmplst

	cd -
done
