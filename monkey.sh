#!/bin/bash

echo "monkey record"

#DEV=$(adb devices -l |grep -v devices | awk '{print $3}')
DEV=$(adb devices -l |grep "$1"| grep -v devices | awk '{print $3}')

for dev in $DEV
do
	echo $dev
	adb -s $dev root
	echo "============$2==============" |tee -a $dev
	adb -s $dev shell uptime |tee -a $dev
	adb -s $dev shell cat proc/cmdline |tee -a $dev
done

echo "record done"

