#!/bin/bash

inFile=$1


# 使用一个临时文件来存储中间结果
temp_file=$(mktemp)

# 读取文件路径，提取文件名并去掉 .exe 后缀
while read -r file; do
    basename "$file" .exe
done < $inFile | sort | uniq > "$temp_file"

# 初始化一个空的字符串变量用于存储结果
result=""

# 读取临时文件并连接文件名
while read -r filename; do
    if [ -z "$result" ]; then
        result="$filename"
    else
        result="$result|$filename"
    fi
done < "$temp_file"

# 删除临时文件
rm "$temp_file"

# 打印结果
echo "$result"

