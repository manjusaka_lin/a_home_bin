mkdir /root/module-signing
cd /root/module-signing
openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=VirtualBox/"
#2) Sign the modules to the kernel

/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxdrv)
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxnetflt)
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxnetadp)
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxpci)
#3) Use mokutil utily to do a request for enrolling the key so it can be trusted by the system and set a simple password
mokutil --import /root/module-signing/MOK.der
#4) Reboot the system and enroll the key (Enroll MOK).



These were the steps I followed, in order to solve it:

1) Create a personal public/private RSA key pair which will be used to sign kernel modules

mkdir /root/module-signing
cd /root/module-signing
openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=VirtualBox/"
2) Sign the modules to the kernel

/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxdrv)
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxnetflt)
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxnetadp)
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vboxpci)
3) Use mokutil utily to do a request for enrolling the key so it can be trusted by the system and set a simple password

mokutil --import /root/module-signing/MOK.der

4) Reboot the system and enroll the key (Enroll MOK).

reboot

5) Do the following changes to the /usr/lib/virtualbox/vboxdrv.sh script

Backup the script: vboxdrv.sh cp -p /usr/lib/virtualbox/vboxdrv.sh /usr/lib/virtualbox/vboxdrv.sh.orig
Edit the script: /usr/lib/virtualbox/vboxdrv.sh
Comment lines: 98,99
#DEB_PUB_KEY=/var/lib/shim-signed/mok/MOK.der
#DEB_PRIV_KEY=/var/lib/shim-signed/mok/MOK.priv
Change for:
DEB_PUB_KEY=/root/module-signing/MOK.der
DEB_PRIV_KEY=/root/module-signing/MOK.priv
Comment lines: 501,503
#kmodsign sha512 /var/lib/shim-signed/mok/MOK.priv \
# /var/lib/shim-signed/mok/MOK.der \
# /lib/modules/"$KERN_VER"/misc/"$i".ko
---
Change for:
/usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 /root/module-signing/MOK.priv \
/root/module-signing/MOK.der \
/lib/modules/"$KERN_VER"/misc/"$i".ko
6) Run the command "/usr/lib/virtualbox/vboxdrv.sh setup"

root@mipc:~# /usr/lib/virtualbox/vboxdrv.sh setup
vboxdrv.sh: Stopping VirtualBox services.
vboxdrv.sh: Starting VirtualBox services.
vboxdrv.sh: Building VirtualBox kernel modules.
No DKMS packages installed: not changing Secure Boot validation state.
It will print the following some instructions you have to follow, it will probably ask you for the password you set when import MOK.der key in step (3)


6) verify the modules are loaded:

root@mipc:~# lsmod | grep box
vboxpci 28672 0
vboxnetadp 28672 0
vboxnetflt 32768 0
vboxdrv 487424 3 vboxpci,vboxnetadp,vboxnetflt
7) Before running virtualbox remember download the extention pack for the current virtualbox version.

8) With the local user run virtualbox

I hope it could helps you.
Thank you everyone for commenting.
