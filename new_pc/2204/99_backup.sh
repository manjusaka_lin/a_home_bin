#链接：https://www.jianshu.com/p/fa3a3b918d89
echo "do backup"
#tar cvpjf backup.tar.bz2 –exclude=/proc –exclude=/lost+found –exclude=/backup.tar.bz2 –exclude=/mnt –exclude=/sys /
echo "do restore"
#tar xvpfz backup.tgz -C /
#mkdir proc
# mkdir lost+found
# mkdir mnt
# mkdir sys
#https://blog.csdn.net/sunny0660/article/details/79274642
#fdisk -l

#1显示分区情况：
#其中boot分区为/dev/sda5,home分区为/dev/sda7,主分区/为/dev/sda8。

#2分别进行挂载：
#mount /dev/sda5 /boot
#mount /dev/sda7 /home
#mount /dev/sda8 /mnt

#3再对移动硬盘进行挂载：
#mount /media/ubuntu/移动硬盘对应盘符 /data

#4进行备份：
#mksquashfs /mnt /data/ubuntu_main.sfs(文件名任意)
#mksquashfs /home /data/ubuntu_home.sfs(文件名任意)
#mksquashfs /boot /data/ubuntu_boot.sfs(文件名任意)
#sync(让系统保存一下数据)

#######################################################################################################
#restore
#假设分区情况如上：/dev/sda1为boot分区，/dev/sda2为主分区，/dev/sda3为home分区。对home分区和主分区进行格式化：
#sudo su

#mkfs.ext4 /dev/sda2

#mkfs.ext4 /dev/sda3

#然后分别进行挂载：
#
##mount /dev/sda2 /mnt
#
#新建home和boot文件:
#
##mkdir /mnt/home
#
##mkdir /mnt/boot
#
#挂载其他两个盘：
#
##mount /dev/sda1 /mnt/boot
#
##mount /dev/sda3 /mnt/home
#
#挂载数据盘：
#
##mkdir /rescovery/mnt
#
##mkdir /rescovery/home
#
##mkdir /rescovery/boot
#
##mount -o loop /media/ubuntu/移动硬盘盘符/ubuntu_main.sfs /rescovery/mnt
#
##mount -o loop /media/ubuntu/移动硬盘盘符/ubuntu_home.sfs /rescovery/home
#
##mount -o loop /media/ubuntu/移动硬盘盘符/ubuntu_boot.sfs /rescovery/boot
#
#复制文件：
#
##cp -a /recovery/mnt/* /mnt
#
##cp -a /recovery/home/* /mnt/home
#
##cp -a /recovery/boot/* /mnt/boot
#
#然后拷贝之前的fstab和grub.cfg文件到硬盘：
#
##cp /media/ubuntu/移动硬盘盘符/fstab /mnt/etc/
#
##cp /media/ubuntu/移动硬盘盘符/grub.cfg /mnt/boot/grub/
#
#挂载虚拟文件系统，这是为了后面修复引导做准备。
#
##mount --o bind /dev /mnt/dev
#
##mount --o bind /proc /mnt/proc
#
##mount --o bind /sys /mnt/sys
#
#chroot进入已经还原的操作系统。
#
##chroot /mnt
#
#查看当前UUID
#
#由于我们格式化了分区，所以UUID发生了变化，若不修改，系统将无法正常挂载分区，导致启动异常。故需要修改本机系统的UUID设置,当前终端不要关闭，新建一个终端，输入blkid:
#对fstab文件进行格式化两个盘的UUID的更改,在chroot过的端口输入：
#
##nano /etc/fstab
#
#更改两个UUID，ctrl+x退出，Y保存。进行grub的更新：
#
##grub-install /dev/sda
#
##update-grub
#
#退出并卸载盘：
#
##exit
#
##umount /mnt/dev
#
##umount /mnt/sys
#
##umount /proc
#
##sync
##重启即可

 
