sudo sh -c "echo DefaultTimeoutStartSec=60s >> /etc/systemd/system.conf"; sudo sh -c "echo DefaultTimeoutStopSec=35s >> /etc/systemd/system.conf"
##for new disk setup, the disk is mount at /media/xxx
#first we let adm group have the owner ship, then let adm group have write. and next the user have administor have the write permission
#sudo chgrp adm /media/xxx
#sudo chmod g+w /media/xxx

python 01_custom_key.py 'nautilus' '/usr/bin/nautilus' '<Alt>e'
python 01_custom_key.py 'terminator' '/usr/bin/terminator' '<Alt>q'
python 01_custom_key.py 'xmind' 'bash ~/bin/run_xmind.sh' '<Alt>x'
python 01_custom_key.py 'studio' 'bash ~/bin/run_studio.sh' '<Alt>z'

##open top tubiao
echo "gnome-tweaks, then select extensions and open topicons plugs"
gnome-tweaks

#for logcat-color
pip install logcat-color
ln -s ~/bin/logcat-color ~/.logcat-color
#(echo "Y") | sudo apt-get install thunderbird-locale-zh-cn

#search for the latest kernel version
apt-cache search linux-image
#(echo "Y") | sudo apt-get install linux-modules-extra-5.13.0-21-generic linux-headers-5.13.0-21-generic linux-image-5.13.0-21-generic

sudo apt-get install smartmontools
#echo "GB Written: $(echo "scale=3; $(sudo /usr/sbin/smartctl -A /dev/sda | grep "Total_LBAs_Written" | awk '{print $10}') * 512 / 1073741824" | bc | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')"



mkdir -p ~/work_setup/
cd ~/work_setup/
#git clone https://manjusaka_lin@bitbucket.org/manjusaka_lin/a_home_bin.git
#git clone https://manjusaka_lin@bitbucket.org/manjusaka_lin/a_work_tools.git

ln -s ~/work_setup/a_home_bin ~/bin
#mv ~/.bashrc ~/bk_bashrc
#ln -s ~/bin/bashrc ~/.bashrc

sudo chown root:root ~/bin/adb
sudo chown root:root ~/bin/fastboot
sudo chown root:root ~/bin/wermit

sudo chmod 4755 ~/bin/adb
sudo chmod 4755 ~/bin/fastboot
sudo chmod 4755 ~/bin/wermit

mkdir -p ~/.android
ln -s ~/bin/usb_adb.ini ~/.android/usb_adb.ini
ln -s ~/bin/kermrc ~/.kermrc
ln -s ~/bin/sprdrc ~/sprdrc
ln -s ~/bin/mtkrc ~/mtkrc
ln -s ~/bin/rkrc ~/rkrc
cd ~
ln -s ~/bin/1mtkrc 
ln -s ~/bin/2mtkrc 
ln -s ~/bin/3mtkrc 
ln -s ~/bin/4mtkrc 
ln -s ~/bin/5mtkrc 
cd -
#echo "source ~/bin/bashrc" >> ~/.bashrc

#for jbuild
(echo "Y") |sudo apt install python-cairo
#for python install in 2004
(echo "Y") |pip install python-jenkins
(echo "Y") |pip install jira
(echo "Y") |pip install xlrd
(echo "Y") |pip install xlwt
(echo "Y") |pip install xlutils
(echo "Y") |pip install pexpect
(echo "Y") |pip install xlsxwriter
(echo "Y") |pip install configparser
(echo "Y") |sudo apt-get install python-ldap
(echo "Y") |sudo apt install libssl-dev vim
(echo "Y") |sudo apt-get install ctags cscope
(echo "Y") |sudo apt-get install scrcpy

(echo "Y") |sudo apt-get install fcitx-frontend-qt4

#for git push 
#git config --global credential.helper store

#for python qt
(echo "Y") |sudo apt-get install libcanberra-gtk-module

#for https://github.com/zq1997/deepin-wine sh run error
(echo "Y") |sudo apt install ca-certificates

(echo "Y") |sudo apt-get install device-tree-compiler
(echo "Y") |sudo apt install curlftpfs
(echo "Y") |sudo apt install vim-gui-common
(echo "Y") |sudo apt install ckermit
#(echo "Y") |sudo apt install cscope

cp ~/bin/new_pc/qtcreater.desktop ~/.local/share/applications/
cp ~/bin/new_pc/qtcreater.desktop /home/bsp/.local/share/applications/
cp ~/bin/new_pc/mtknnfl1952.desktop /home/bsp/.local/share/applications/
cp ~/bin/new_pc/mtkv6_2316.desktop /home/bsp/.local/share/applications/
cp ~/bin/new_pc/mtkv52316.desktop /home/bsp/.local/share/applications/
cp ~/bin/new_pc/ch32v103.desktop /home/bsp/.local/share/applications/
sudo update-desktop-database

git config --global credential.helper store
sudo adduser $(whoami) dialout

sudo apt-add-repository ppa:umang/indicator-stickynotes
sudo apt update
sudo apt install indicator-stickynotes
sudo apt install socat

sudo apt remove brltty #for ub2204 ch341 not work ttyusb

echo "Y" | sudo apt-get install python-pip3
pip3 install flask mysql-connector-python

sudo apt-get install exuberant-ctags

pip2 install pypinyin
#for qt creater online
(echo "Y") |sudo apt install libxcb-cursor0 libxcb-cursor-dev 
sudo snap install dbeaver-ce
