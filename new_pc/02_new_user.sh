
bash ./2004/3.sh
bash ./2004/5_gitcode_local.sh
#for python plugins
bash ./2004/2.sh

#for csopco build
#/usr/bin/ld: cannot find -lncurses
#missing libncurses
sudo apt-get install libncurses-dev


#for vim function highlight
tfile=$(find /usr/share/vim -name "c.vim" | grep syntax)
echo $tfile
cp $tfile .
tmpfile=./c.vim
echo '""highlight Functions' >> ${tmpfile}
echo 'syn match cFunctions "\<[a-zA-Z_][a-zA-Z_0-9]*\>[^()]*)("me=e-2' >> $tmpfile
echo 'syn match cFunctions "\<[a-zA-Z_][a-zA-Z_0-9]*\>\s*("me=e-1' >> ${tmpfile}
echo 'hi cFunctions gui=NONE cterm=bold  ctermfg=blue' >> ${tmpfile} 
sudo cp ./c.vim $tfile
rm ./c.vim
