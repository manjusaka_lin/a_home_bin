#/bin/sh
tname=$1
cd ~
mkdir boot1
mkdir boot2
mkdir boot3
mkdir boot4

mkdir datadir;
sudo mount /dev/sdb1 datadir
ww=$(whoami)
sudo chown  $ww:$ww datadir -R
sudo mount /dev/sda1 boot1
sudo mount /dev/sda2 boot2
sudo mount /dev/sda3 boot3
sudo mount /dev/sda4 boot4
sudo dd if=/dev/sda1 of=~/datadir/${tname}_boot1.dd
#sudo mksquashfs boot1 ~/datadir/${tname}_boot1.sfs
sudo mksquashfs boot2 ~/datadir/${tname}_boot2.sfs
sudo mksquashfs boot3 ~/datadir/${tname}_boot3.sfs
sudo mksquashfs boot4 ~/datadir/${tname}_boot4.sfs
