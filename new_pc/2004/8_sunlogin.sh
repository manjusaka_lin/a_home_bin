sudo apt install libegl1-mesa
mkdir ~/tmp/
cd ~/tmp/
wget http://download.oray.com/sunlogin/linux/SunloginClient-10.0.2.24779_amd64.deb

wget http://mirrors.aliyun.com/ubuntu/pool/main/i/icu/libicu60_60.2-3ubuntu3_amd64.deb
wget http://mirrors.aliyun.com/ubuntu/pool/universe/w/webkitgtk/libjavascriptcoregtk-3.0-0_2.4.11-3ubuntu3_amd64.deb
wget http://mirrors.aliyun.com/ubuntu/pool/universe/m/mesa/libegl1-mesa_20.0.4-2ubuntu1_amd64.deb
wget http://mirrors.aliyun.com/ubuntu/pool/universe/e/enchant/libenchant1c2a_1.6.0-11.3build1_amd64.deb
wget http://mirrors.aliyun.com/ubuntu/pool/universe/w/webkitgtk/libwebkitgtk-3.0-0_2.4.11-3ubuntu3_amd64.deb

sudo dpkg -i libicu60_60.2-3ubuntu3_amd64.deb
sudo dpkg -i libjavascriptcoregtk-3.0-0_2.4.11-3ubuntu3_amd64.deb
sudo dpkg -i libegl1-mesa_20.0.4-2ubuntu1_amd64.deb
sudo dpkg -i libenchant1c2a_1.6.0-11.3build1_amd64.deb
sudo dpkg -i libwebkitgtk-3.0-0_2.4.11-3ubuntu3_amd64.deb
sudo dpkg -i SunloginClient-10.0.2.24779_amd64.deb
cd -
