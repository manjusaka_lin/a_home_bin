#/bin/sh
TDIR=$1
cat MT*.txt | grep "\.bin\|\.img" | awk -F":" '{print $2}' | sort -u > flist;
cat flist | while read line
do
    echo $line
    cp $line $TDIR
    result=$(echo "$line" | awk -F '.' '{print $1}')
    cp $result* $TDIR;
done
cp MT*.txt $TDIR
cp MT*.xml $TDIR
cp -rf download_agen* $TDIR/
cp ltk.img $TDIR
cp scatter_checksum.xml $TDIR
cp CheckSum_Generate.exe $TDIR
cp imageChecker.dll $TDIR
#cp vbmeta*.img $TDIR;
#cp super.img $TDIR;
#cp boot.img $TDIR;
#cp userdata.img $TDIR;
