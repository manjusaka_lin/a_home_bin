
ORI_PRO=$1
NEW_PRO=$2

cd kernel-4.19
cp -rf ./drivers/misc/mediatek/dws/mt6853/$ORI_PRO.dws ./drivers/misc/mediatek/dws/mt6853/$NEW_PRO.dws
cp -rf ./arch/arm64/boot/dts/mediatek/$ORI_PRO.dts ./arch/arm64/boot/dts/mediatek/$NEW_PRO.dts
cp -rf ./arch/arm64/configs/${ORI_PRO}_defconfig ./arch/arm64/configs/${NEW_PRO}_defconfig
cp -rf ./arch/arm64/configs/${ORI_PRO}_debug_defconfig ./arch/arm64/configs/${NEW_PRO}_debug_defconfig

cd -
rm -rf device/mediateksample/${NEW_PRO}/
mkdir device/mediateksample/${NEW_PRO}/
cp -rf device/mediateksample/${ORI_PRO}/*  device/mediateksample/${NEW_PRO}/
find device/mediateksample/${NEW_PRO} -type f -exec sed -i "s/${ORI_PRO}/${NEW_PRO}/g" {} +
cd device/mediateksample/${NEW_PRO} 
rename "s/${ORI_PRO}/${NEW_PRO}/g" *
cd -


rm -rf vendor/mediatek/proprietary/custom/${NEW_PRO}/
mkdir vendor/mediatek/proprietary/custom/${NEW_PRO}/
cp -rf vendor/mediatek/proprietary/custom/${ORI_PRO}/*  vendor/mediatek/proprietary/custom/${NEW_PRO}/

find vendor/mediatek/proprietary/custom/${NEW_PRO} -type f -exec sed -i "s/${ORI_PRO}/${NEW_PRO}/g" {} +


cp vendor/mediatek/proprietary/bootable/bootloader/lk/project/${ORI_PRO}.mk  vendor/mediatek/proprietary/bootable/bootloader/lk/project/${NEW_PRO}.mk 
sed -i "s/${ORI_PRO}/${NEW_PRO}/g" vendor/mediatek/proprietary/bootable/bootloader/lk/project/${NEW_PRO}.mk

rm -rf vendor/mediatek/proprietary/bootable/bootloader/lk/target/${NEW_PRO}
mkdir -p vendor/mediatek/proprietary/bootable/bootloader/lk/target/${NEW_PRO}
cp -rf vendor/mediatek/proprietary/bootable/bootloader/lk/target/${ORI_PRO}/* vendor/mediatek/proprietary/bootable/bootloader/lk/target/${NEW_PRO}/

rm -rf vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${NEW_PRO}
mkdir -p vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${NEW_PRO}
cp -rf vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${ORI_PRO}/* vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${NEW_PRO}/

find vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${NEW_PRO} -type f -exec sed -i "s/${ORI_PRO}/${NEW_PRO}/g" {} +
cd vendor/mediatek/proprietary/bootable/bootloader/preloader/custom/${NEW_PRO} 
rename "s/${ORI_PRO}/${NEW_PRO}/g" *
cd -



mkdir -p vendor/mediatek/proprietary/tinysys/adsp/HIFI3/project/mt6877/HIFI3_A/${NEW_PRO}
cp -rf vendor/mediatek/proprietary/tinysys/adsp/HIFI3/project/mt6877/HIFI3_A/${ORI_PRO}/* vendor/mediatek/proprietary/tinysys/adsp/HIFI3/project/mt6877/HIFI3_A/${NEW_PRO}/


mkdir -p vendor/mediatek/proprietary/tinysys/scp/project/RV55_A/mt6853/${NEW_PRO}
cp -rf vendor/mediatek/proprietary/tinysys/scp/project/RV55_A/mt6853/${ORI_PRO}/* vendor/mediatek/proprietary/tinysys/scp/project/RV55_A/mt6853/${NEW_PRO}/


cp -rf vendor/mediatek/proprietary/trustzone/custom/build/project/${ORI_PRO}.mk  vendor/mediatek/proprietary/trustzone/custom/build/project/${NEW_PRO}.mk
